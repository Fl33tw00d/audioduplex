# Secure VOIP (Voice Over IP) System with Packet Loss Compensation

In this project I have designed an encrypted Voice Over IP system that features mutliple compensation methods in order to overcome the introduced errors intentionally introduced in the altered
DatagramSockets that are included in the VOIP.jar file. In addition I also wrote a paper explaining how the system works and how I would take it further to really achieve a fully secure duplex communication system.

I will summarize the report points here:
DatagramSocket1
DatagramSocket1 is the built in Java DatagramSocket that required no compensation.

DatagramSocket2
DatagramSocket2 had simulated long bursts of packet loss.
This was solved using a number of methods:
4x4 block interleaver at the sender to create spread in adjacent packet sequence numbers.
Compression of previous packet into the current packet header from 512 bytes to 128 bytes to allow for 
packet reconstruction on the receiver side if a loss was detected.
Nearest neighbour repetition.

DatagramSocket3
DatagramSocket3 had simulated shorter but more frequent bursts of packet loss that really hindered speech intelligibility.
This was solved using the same methods as DatagramSocket2 but altered to fit the socket profile better.

DatagramSocket4
DatagramSocket 4 had simulated corrupt packets. About 12% of the packets were corrupted in transmission causing
loud audio spikes that were very noticeable.
This was solved by hashing the packet before sending and putting the result in the packet header.
This was used to verify the authenticity of the packets at the receiver side.
If the packet was corrupted then it was discarded and we used basic repetition to fill the gap created.

## Authors

* **Christopher Fleetwood** -[Fl33tw00d](https://bitbucket.com/Fl33tw00d)

## Acknowledgments

* StackOverflow
* RTP Packet Design
