/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voip;

import CMPC3M06.AudioRecorder;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

/**
 *
 * @author vhe17qgu
 */
public class Sender4 implements Runnable {

    static DatagramSocket sending_socket;
    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        //Port to send to
        int PORT = 55555;
        //IP ADDRESS to send to
        InetAddress clientIP = null;
        try {
            clientIP = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            System.out.println("Could not connect");
            e.printStackTrace();
            System.exit(0);
        }

        try {
            sending_socket = new DatagramSocket4();
        } catch (SocketException e) {
            System.out.println("");
            e.printStackTrace();
            System.exit(0);
        }

        //Main loop
        boolean running = true;

        AudioRecorder recorder = null;
        try {
            recorder = new AudioRecorder();
            sending_socket.setSoTimeout(5000);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(Sender1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SocketException ex) {
            Logger.getLogger(Sender1.class.getName()).log(Level.SEVERE, null, ex);
        }

        int sequenceNo = 0;
        long startTime = System.nanoTime();
        while (running && sequenceNo <= 100) {
            try {
                    ByteBuffer wrappedBlock = ByteBuffer.allocate(528);
                    byte[] audioBlock = recorder.getBlock();
                    int hashKey = Arrays.hashCode(audioBlock);
                    long time = System.currentTimeMillis();
                    wrappedBlock.putInt(sequenceNo);
                    wrappedBlock.putInt(hashKey);
                    wrappedBlock.putLong(time);
                    wrappedBlock.put(audioBlock);
                    DatagramPacket packet = new DatagramPacket(wrappedBlock.array(), wrappedBlock.array().length, clientIP, PORT);
                    sequenceNo++;
                    sending_socket.send(packet);
            } catch (IOException e) {
                System.out.println("ERROR: " + e);
                e.printStackTrace();
            }
        }
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println("Sender Delay: " + duration /sequenceNo);
        sending_socket.close();
    }
    
    
}

