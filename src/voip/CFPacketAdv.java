/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voip;

import java.nio.ByteBuffer;

/** 
 *
 * @author fleet
 */
public class CFPacketAdv implements Comparable{

    private int sequenceNo;
    private int hashKey;
    private long timeStamp;
    private byte[] compressedAudio;
    private byte[] audio;
    
    
    //NULL PACKET CONSTRUCTOR
    public CFPacketAdv(){
        sequenceNo = -1;
        hashKey = -1;
        timeStamp = -1;
        compressedAudio = new byte[128];
        audio = new byte[512];
    }
    
    //NORMAL CONSTRUCTOR
    public CFPacketAdv(byte[] packet){
       ByteBuffer bbuf = ByteBuffer.wrap(packet);
       sequenceNo = bbuf.getInt();
       hashKey = bbuf.getInt();
       timeStamp = bbuf.getLong();
       compressedAudio = new byte [128];
       bbuf.get(compressedAudio,0,128);
       audio = new byte[512];
       bbuf.get(audio,0,512);
    }
    //COPY CONSTRUCTOR
    public CFPacketAdv(CFPacketAdv different){
        this.sequenceNo = different.sequenceNo;
        this.hashKey = different.hashKey;
        this.timeStamp = different.timeStamp;
        this.compressedAudio = different.compressedAudio;
        this.audio = different.audio;
    }
    
    @Override
    public int compareTo(Object o) {
        return Integer.compare(sequenceNo, ((CFPacketAdv)o).getSequenceNo());
    }

    /**
     * @return the sequenceNo
     */
    public int getSequenceNo() {
        return sequenceNo;
    }

    /**
     * @param sequenceNo the sequenceNo to set
     */
    public void setSequenceNo(int sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    /**
     * @return the hashKey
     */
    public int getHashKey() {
        return hashKey;
    }

    /**
     * @param hashKey the hashKey to set
     */
    public void setHashKey(int hashKey) {
        this.hashKey = hashKey;
    }

    /**
     * @return the timeStamp
     */
    public long getTimeStamp() {
        return timeStamp;
    }

    /**
     * @param timeStamp the timeStamp to set
     */
    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    /**
     * @return the audio
     */
    public byte[] getAudio() {
        return audio;
    }

    /**
     * @param audio the audio to set
     */
    public void setAudio(byte[] audio) {
        this.audio = audio;
    }
    
    public void setCompressedAudio(byte[] compAudio){
        this.compressedAudio = compAudio;
    }
    
    public byte[] uncompressAudio(){
        ByteBuffer uncompressedAudio = ByteBuffer.allocate(512);
        ByteBuffer comp = ByteBuffer.wrap(compressedAudio);
        
        for (int i = 0; i < 64; i++) {
            short soundSample;
            soundSample = comp.getShort();
            for (int j = 0; j < 4; j++) {
                uncompressedAudio.putShort(soundSample);
            }
        }
        return uncompressedAudio.array();
    }
    
    
}
