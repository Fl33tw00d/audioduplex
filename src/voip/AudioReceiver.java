/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voip;

import CMPC3M06.AudioPlayer;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

/**
 *
 * @author vhe17qgu
 */
public class AudioReceiver implements Runnable {

    DatagramSocket receiving_socket;
    AudioPlayer player;
    DatagramPacket packet;
    DatagramPacket[] sortedPackets;
    short receivedHashCode;

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        try {
            DatagramSocket1();
            // DatagramSocket2();
            //DatagramSocket3();
            // DatagramSocket4();
        } catch (IOException ex) {
            Logger.getLogger(AudioReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
        public void DatagramSocket1() throws IOException {
        int PORT = 55555;
        try {
            receiving_socket = new DatagramSocket(PORT);
        } catch (SocketException e) {
            System.out.println("ERROR: Could not open UDP socket to receive from.");
            e.printStackTrace();
            System.exit(0);
        }

        boolean running = true;

        try {
            player = new AudioPlayer();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(AudioReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }
        short expectedSeq = 0;
        while (running) {
                byte[] AudioBytes = new byte[512];
                packet = new DatagramPacket(AudioBytes, 0, AudioBytes.length);
                receiving_socket.receive(packet);
                player.playBlock(AudioBytes);
//            try {
//                //Getting data from the packet
//                byte[] tempbuf = new byte[514];
//                byte[] AudioBytes = new byte[512];
//                byte[] prevAudioBytes = new byte[512];
//                sortedPackets = new DatagramPacket[16];
//                short receivedSeq = 0;
//
//                for (int i = 0; i < 4; i++) {
//                    for (int j = 0; j < 4; j++) {
//                        packet = new DatagramPacket(tempbuf, 0, tempbuf.length);
//                        receiving_socket.receive(packet);
//                        sortedPackets[j * 4 + (4 - 1 - i)] = packet;
//                    }
//                }
//
//                for (int i = 0; i < 16; i++) {
//                    byte[] udpPacketBytes = sortedPackets[i].getData();
//                    ByteBuffer bb = ByteBuffer.wrap(udpPacketBytes);
//                    receivedSeq = bb.getShort();
//                    System.out.println("RECEIVED: " + receivedSeq);
//                    bb.get(AudioBytes, 0, 512);
//                    player.playBlock(AudioBytes);
//                }
//
//            } catch (IOException e) {
//                System.out.println("No output device connected.");
//                e.printStackTrace();
//            }
        }

        receiving_socket.close();
    }

    public void DatagramSocket2() {
        int PORT = 55555;
        try {
            receiving_socket = new DatagramSocket(PORT);
        } catch (SocketException e) {
            System.out.println("ERROR: Could not open UDP socket to receive from.");
            e.printStackTrace();
            System.exit(0);
        }

        boolean running = true;

        try {
            player = new AudioPlayer();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(AudioReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }
        short expectedSeq = 0;
        while (running) {
            try {
                //Getting data from the packet
                byte[] tempbuf = new byte[514];
                byte[] AudioBytes = new byte[512];
                byte[] prevAudioBytes = new byte[512];
                sortedPackets = new DatagramPacket[16];
                short receivedSeq = 0;

                for (int i = 0; i < 4; i++) {
                    for (int j = 0; j < 4; j++) {
                        packet = new DatagramPacket(tempbuf, 0, tempbuf.length);
                        receiving_socket.receive(packet);
                        sortedPackets[j * 4 + (4 - 1 - i)] = packet;
                    }
                }

                for (int i = 0; i < 16; i++) {
                    byte[] udpPacketBytes = sortedPackets[i].getData();
                    ByteBuffer bb = ByteBuffer.wrap(udpPacketBytes);
                    receivedSeq = bb.getShort();
                    System.out.println("RECEIVED: " + receivedSeq);
                    bb.get(AudioBytes, 0, 512);
                    player.playBlock(AudioBytes);
                }

            } catch (IOException e) {
                System.out.println("No output device connected.");
                e.printStackTrace();
            }
        }

        receiving_socket.close();
    }

    public void DatagramSocket3() {

    }

    public void DatagramSocket4() {
        int PORT = 55555;
        try {
            receiving_socket = new DatagramSocket4(PORT);
        } catch (SocketException e) {
            System.out.println("ERROR: Could not open UDP socket to receive from.");
            e.printStackTrace();
            System.exit(0);
        }

        boolean running = true;

        try {
            player = new AudioPlayer();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(AudioReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }
        while (running) {
            try {
                //Getting data from the packet
                byte[] tempbuf = new byte[514];
                byte[] AudioBytes = new byte[512];

                packet = new DatagramPacket(tempbuf, 0, tempbuf.length);
                receiving_socket.receive(packet);
                byte[] udpPacketBytes = packet.getData();
                ByteBuffer bb = ByteBuffer.wrap(udpPacketBytes);
                receivedHashCode = bb.getShort();
                bb.get(AudioBytes, 0, 512);
                if(receivedHashCode == (short) Arrays.hashCode(AudioBytes)){
                    player.playBlock(AudioBytes);
                }


            } catch (IOException e) {
                System.out.println("No output device connected.");
                e.printStackTrace();
            }
        }

        receiving_socket.close();
    }
}

//                byte[] udpPacketBytes = packet.getData();
//                ByteBuffer bb = ByteBuffer.wrap(udpPacketBytes);
//
//                receivedSeq = bb.getShort();
//                if (receivedSeq > expectedSeq) {
//                        bb.get(AudioBytes, 0, 512);
//                        player.playBlock(AudioBytes);
//                        prevAudioBytes = AudioBytes;
//                } else {
//                    player.playBlock(prevAudioBytes);
//                }
