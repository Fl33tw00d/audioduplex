/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voip;

import CMPC3M06.AudioPlayer;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

/**
 *
 * @author vhe17qgu
 */
public class Receiver1 implements Runnable {

    static DatagramSocket receiving_socket;
    short authK = 32000;
    int XORKey = (int) DHSecurity.generateKey();
    long durationSum;
    
    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        int PORT = 55555;
        try {
            receiving_socket = new DatagramSocket(PORT);
            receiving_socket.setSoTimeout(5000);
        } catch (SocketException e) {
            System.out.println("ERROR: Could not open UDP socket to receive from.");
            e.printStackTrace();
            System.exit(0);
        }
        
        boolean running = true;
        AudioPlayer player = null;

        try {
            player = new AudioPlayer();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(Receiver1.class.getName()).log(Level.SEVERE, null, ex);
        }
        int count = 0;
         while (running && count <= 100) {
            try {
                long startTime = System.nanoTime();
                //Getting data from the packet
                byte[] buffer = new byte[530];
                byte[] packetData = new byte[530];
                DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);

                try {
                    receiving_socket.receive(packet);
                } catch (SocketTimeoutException e) {
                    System.out.println("Socket Timed Out");
                }
                
                packetData = DHSecurity.XORDecryptInt(packet.getData(),XORKey);
                CFPacketSecure inspPacket = new CFPacketSecure(packetData);
               // CFPacketSecure inspPacket = new CFPacketSecure(packet.getData());
                System.out.println("Seq: " + inspPacket.getSequenceNo());
                if (inspPacket.getAuthKey() == authK) {
                    player.playBlock(inspPacket.getAudio());        
                } 
                
                long endTime = System.nanoTime();
                long duration = (endTime - startTime);
                durationSum += duration;
                count++;
            } catch (IOException e) {
                System.out.println("No output device connected.");
                e.printStackTrace();
            }
            
        }
         System.out.println("duration rec: " + durationSum / count);
//                Collections.sort(packetList);
//        for(int i = 0; i < packetList.size()-1;i++) {
//            if (packetList.get(i+1).getSequenceNo() - packetList.get(i).getSequenceNo() != 1) {
//                burstLength = packetList.get(i+1).getSequenceNo() - packetList.get(i).getSequenceNo() - 1;
//                System.out.println("BURST: " + burstLength);
//                allBursts += burstLength;
//                numBursts++;
//            }
//        }
        
        
//        System.out.println("Avg burst length = " + allBursts/numBursts);     
        
        receiving_socket.close();
    }

}
//          DatagramSocket3 burstlength
//        Collections.sort(packetList);
//        for(int i = 0; i < packetList.size();i++) {
//            if (packetList.get(i+1).getSequenceNo() - packetList.get(i).getSequenceNo() != 1) {
//                burstLength = packetList.get(i+1).getSequenceNo() - packetList.get(i).getSequenceNo() - 1;
//                allBursts += burstLength;
//                numBursts++;
//            }
//        }



//                  DatagramSocket2 testing 
//                CFPacket inspPacket = new CFPacket(packet.getData());
//                if (inspPacket.getSequenceNo() - previousPacket.getSequenceNo() != 1) {
//                    burstLength = inspPacket.getSequenceNo() - previousPacket.getSequenceNo() - 1;
//                    System.out.println("BURST: " + burstLength);
//                    allBursts += burstLength;
//                    numBursts++;
//                }
//                long delay = System.currentTimeMillis() - inspPacket.getTimeStamp();
//                System.out.println("Seq: " + inspPacket.getSequenceNo());
////                System.out.println("HashKey: " + inspPacket.getHashKey());
////                System.out.println("Time Sent: " + inspPacket.getTimeStamp());
//                if (inspPacket.getHashKey() == Arrays.hashCode(inspPacket.getAudio())) {
//                    player.playBlock(inspPacket.getAudio());        
//                    previousPacket.setSequenceNo(inspPacket.getSequenceNo());
////                    System.out.println("No. of Packets Received: " + packetsReceived++);   
////                    System.out.println("Delay: " + delay);
//                }
//                
//                if(inspPacket.getSequenceNo() == 1000){
//                    break;
//                }