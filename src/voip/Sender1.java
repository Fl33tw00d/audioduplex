/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voip;

import CMPC3M06.AudioRecorder;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

/**
 *
 * @author vhe17qgu
 */
public class Sender1 implements Runnable {

    static DatagramSocket sending_socket;
//    int XORKey = (int) DHSecurity.generateKey();
    int XORKey = 196753451;
    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        //Port to send to
        int PORT = 55555;
        //IP ADDRESS to send to
        InetAddress clientIP = null;
        try {
            clientIP = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            System.out.println("Could not connect");
            e.printStackTrace();
            System.exit(0);
        }
        
        

        try {
            sending_socket = new DatagramSocket();
        } catch (SocketException e) {
            System.out.println("");
            e.printStackTrace();
            System.exit(0);
        }
        
//        //SENDING ONE TIME PAD 
//        byte[] OTP = DHSecurity.generateRandomBytes(528);
//        DHSecurity.sendKey(OTP);
        
        //Main loop
        boolean running = true;

        AudioRecorder recorder = null;
        try {
            recorder = new AudioRecorder();
            sending_socket.setSoTimeout(5000);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(Sender1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SocketException ex) {
            Logger.getLogger(Sender1.class.getName()).log(Level.SEVERE, null, ex);
        }

        int sequenceNo = 0;
        long startTime = System.nanoTime();
        while (running && sequenceNo <= 100) {
            try {
                byte[] audioBlock = recorder.getBlock();
                int hashKey = Arrays.hashCode(audioBlock);
                long time = System.currentTimeMillis();
                short authKey = 32000;
                ByteBuffer wrappedBlock = ByteBuffer.allocate(530);
                wrappedBlock.putInt(sequenceNo);
                wrappedBlock.putInt(hashKey);
                wrappedBlock.putLong(time);
                wrappedBlock.putShort(authKey);
                wrappedBlock.put(audioBlock);
                byte[] packetBytes = wrappedBlock.array();
                packetBytes = DHSecurity.XOREncryptInt(packetBytes,XORKey);
                DatagramPacket packet = new DatagramPacket(packetBytes, packetBytes.length, clientIP, PORT);
                sending_socket.send(packet);
                sequenceNo++;
            } catch (IOException e) {
                System.out.println("ERROR: " + e);
                e.printStackTrace();
            }
        }
                long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println("Sender Delay: " + duration /sequenceNo);
        sending_socket.close();
    }
    

}
