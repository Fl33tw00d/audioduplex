/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voip;

import CMPC3M06.AudioPlayer;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

/**
 *
 * @author vhe17qgu
 */
public class Receiver3Adv implements Runnable {

    static DatagramSocket receiving_socket;
    private int BLOCK_DIMENSION = 4;
    long durationSum;

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        int PORT = 55555;
        try {
            receiving_socket = new DatagramSocket3(PORT);
        } catch (SocketException e) {
            System.out.println("ERROR: Could not open UDP socket to receive from.");
            e.printStackTrace();
            System.exit(0);
        }

        boolean running = true;

        AudioPlayer player = null;
        try {
            player = new AudioPlayer();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(AudioReceiver.class.getName()).log(Level.SEVERE, null, ex);
        }

        ArrayList<CFPacketAdv> receivedPackets = new ArrayList<>();
        ArrayList<CFPacketAdv> packetsToPlay = new ArrayList<>();
        CFPacketAdv previousPacket = null;
        int blockCount = 1;
        //Main Loop
        long startTime = System.nanoTime();
        while (running && blockCount <= 10) {
            try {
                byte[] buffer = new byte[656];

                DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);
                try {
                    receiving_socket.receive(packet);
                } catch (SocketTimeoutException e) {
                    System.out.println("Socket timed out");
                    running = false;
                    break;
                }
                CFPacketAdv inspPacket = new CFPacketAdv(packet.getData());
          //      System.out.println("RECEIVED: " + inspPacket.getSequenceNo());
                if (inspPacket.getSequenceNo() <= (blockCount * BLOCK_DIMENSION * BLOCK_DIMENSION) - 1) {
                    if (inspPacket.getHashKey() == Arrays.hashCode(inspPacket.getAudio())) {
                        receivedPackets.add(inspPacket);
                    }
                } else {
                    if(receivedPackets.size() == 1){
                        break;
                    }
                    previousPacket = inspPacket;
                    Collections.sort(receivedPackets);
                    //REMOVE LOOP
                        if(receivedPackets.get(1).getSequenceNo()
                                - receivedPackets.get(0).getSequenceNo() > 3){
                            receivedPackets.remove(0);
                        }
                    
                    //INTERPOLATION LOOP
                    for (int i = 0; i < receivedPackets.size() - 1; i++) {
                        //System.out.println("RECEIVED: " + receivedPackets.get(i).getSequenceNo());
                        int packetsNeeded = receivedPackets.get(i + 1).getSequenceNo()
                                - receivedPackets.get(i).getSequenceNo() - 1;

                        if (packetsNeeded > 0) {
                            int lower = packetsNeeded - packetsNeeded / 2;
                            for (int j = 0; j <= packetsNeeded; j++) {
                                if (j <= lower) {
                                    packetsToPlay.add(receivedPackets.get(i));
                                } else {
                                    packetsToPlay.add(receivedPackets.get(i + 1));
                                }
                            }
                        } else {
                            packetsToPlay.add(receivedPackets.get(i));
                        }
                    }
                    
                    //Adding any packets where the compressed audio will help
                    for (int i = 0; i < packetsToPlay.size() - 1; i++) {
                    int packetsNeeded = packetsToPlay.get(i + 1).getSequenceNo()
                                - packetsToPlay.get(i).getSequenceNo() - 1;
                        
                        if(packetsNeeded == 1){
                            CFPacketAdv recoveredPacket = new CFPacketAdv(packetsToPlay.get(i+1));
                            byte[] recoveredAudio = recoveredPacket.uncompressAudio();
                            recoveredPacket.setSequenceNo(packetsToPlay.get(i+1).getSequenceNo() - 1);
                            recoveredPacket.setAudio(recoveredAudio);
                            packetsToPlay.set(i, recoveredPacket);
                        }
                    }
                    
                    
                    while(packetsToPlay.size() < BLOCK_DIMENSION * BLOCK_DIMENSION){
                        packetsToPlay.add(receivedPackets.get(receivedPackets.size() - 1));
                    }
                    
                    for (int i = 0; i < BLOCK_DIMENSION * BLOCK_DIMENSION; i++) {
                        player.playBlock(packetsToPlay.get(i).getAudio());
                       System.out.println("Packet Played: " + packetsToPlay.get(i).getSequenceNo());
                    }
                    packetsToPlay.clear();
                    receivedPackets.clear();
                    if (previousPacket.getHashKey() == Arrays.hashCode(previousPacket.getAudio())) {
                        receivedPackets.add(previousPacket);
                    }
                    blockCount++;
                }

            } catch (IOException e) {
                System.out.println("No output device connected.");
                e.printStackTrace();
            }

        }
        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        durationSum = +duration;
        //  System.out.println("Processing Delay: " + duration);
        long avgDuration = durationSum / blockCount - 1;
        System.out.println("Average Duration: " + avgDuration);
        receiving_socket.close();
    }

}

//                    DOESNT WORK AS PREVIOUS PACKET ISNT GUARATNEED TO BE CLOSE TO
//                    int packetsNeeded = previousPacket.getSequenceNo()
//                            - packetsToPlay.get(packetsToPlay.size() - 1).getSequenceNo();
//                    
//                        if (packetsNeeded > 0) {
//                            int lower = packetsNeeded - packetsNeeded / 2;
//                            for (int j = 0; j <= packetsNeeded; j++) {
//                                if (j <= lower) {
//                                    packetsToPlay.add(packetsToPlay.get(packetsToPlay.size() - 1));
//                                } else {
//                                    packetsToPlay.add(previousPacket);
//                                }
//                            }
//                        } else {
//                            packetsToPlay.add(packetsToPlay.get(packetsToPlay.size() - 1));
//                        }





//Sort Packets here
//Play here    
//                    for (int i = 0; i < receivedPackets.size(); i++) {
//                        if (receivedPackets.get(i).getSequenceNo() == expectedSeq) {
//                            packetsToPlay.add(receivedPackets.get(i));
//                            expectedSeq++;
//                        } else {
//                            int packetsNeeded = receivedPackets.get(i).getSequenceNo()
//                                    - receivedPackets.get(i - 1).getSequenceNo() - 1;
//
//                            for (int j = i; j < i + packetsNeeded; j++) {
//                                packetsToPlay.add(receivedPackets.get(i - 1));
//                                expectedSeq = packetsToPlay.get(packetsToPlay.size() - 1).getSequenceNo() + packetsNeeded + 1;
//                                i = i - 1;
//                            }
//                            //THIS DOESN'T WORK
//                            if (packetsNeeded >= 2) {
//                                for (int j = i + packetsNeeded / 2; j < i + packetsNeeded; j++) {
//                                    packetsToPlay.set(j, receivedPackets.get(i + 1));
//                                    expectedSeq = packetsToPlay.get(packetsToPlay.size() - 1).getSequenceNo() + packetsNeeded + 1;
//                                    i = i - 1;
//                                }
//                            }
//                        }
//                for (int i = 0; i < BLOCK_DIMENSION * BLOCK_DIMENSION; i++) {
//                    System.out.println("Received Packet: " + receivedPackets.get(i).getSequenceNo());
//                    if (receivedPackets.get(i).getSequenceNo() == expectedSeq) {
//                        if (receivedPackets.get(i).getHashKey() == Arrays.hashCode(receivedPackets.get(i).getAudio())) {
//                            System.out.println("Played Packet: " + receivedPackets.get(i).getSequenceNo());
//                            player.playBlock(receivedPackets.get(i).getAudio());
//                        }
//                    }else{
//                        System.out.println("Played Packet: " + previousPacket.getSequenceNo());
//                        player.playBlock(previousPacket.getAudio());
//                    }
//                    expectedSeq++;
//                    previousPacket = receivedPackets.get(i);
//                }
// try {
//                receivedPackets = new ArrayList<>();
//                packetsToPlay = new ArrayList<>();
//                for (int i = 0; i < BLOCK_DIMENSION * BLOCK_DIMENSION; i++) {
//                    byte[] buffer = new byte[528];
//
//                    DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);
//                    try {
//                        receiving_socket.receive(packet);
//                    } catch (SocketTimeoutException e) {
//                        System.out.println("Socket timed out");
//                        running = false;
//                        break;
//                    }
//
//                    CFPacket inspPacket = new CFPacket(packet.getData());
//                    if (inspPacket.getHashKey() == Arrays.hashCode(inspPacket.getAudio())) {
//                        
//                        receivedPackets.add(inspPacket);
//                        
//                    }
//                }
//                //Sort Packets here
//                Collections.sort(receivedPackets);
//
//                for (int i = 0; i < receivedPackets.size(); i++) {
//                    if (receivedPackets.get(i).getSequenceNo() == expectedSeq) {
//                        packetsToPlay.add(receivedPackets.get(i));
//                        expectedSeq++;
//                    } else {
//                        int packetsNeeded = receivedPackets.get(i).getSequenceNo()
//                                - receivedPackets.get(i - 1).getSequenceNo() - 1;
//
//                        for (int j = i; j < i + packetsNeeded; j++) {
//                            packetsToPlay.add(receivedPackets.get(i - 1));
//                            expectedSeq = packetsToPlay.get(packetsToPlay.size() - 1).getSequenceNo() + packetsNeeded + 1;
//                            i = i - 1;
//                        }
//                        //THIS DOESN'T WORK
//                        if (packetsNeeded >= 2) {
//                            for (int j = i + packetsNeeded / 2; j < i + packetsNeeded; j++) {
//                                packetsToPlay.set(j, receivedPackets.get(i + 1));
//                                expectedSeq = packetsToPlay.get(packetsToPlay.size() - 1).getSequenceNo() + packetsNeeded + 1;
//                                i = i - 1;
//                            }
//                        }
//                    }
//                }
//
//            } catch (IOException e) {
//                System.out.println("No output device connected.");
//                e.printStackTrace();
//            }
//            receiving_socket.close();
//        }
