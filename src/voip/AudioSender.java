package voip;

import CMPC3M06.AudioRecorder;
import java.io.IOException;
import static java.lang.Integer.MAX_VALUE;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author vhe17qgu
 */
public class AudioSender implements Runnable {

    AudioRecorder recorder;
    DatagramSocket sending_socket;
    ArrayList<DatagramPacket> packetList;
    short seqNum;
    short sentHashCode;

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        DatagramSocket1();
       // DatagramSocket2();
      // DatagramSocket3();
        //DatagramSocket4();
    }
    
        public void DatagramSocket1() {
        //Port to send to
        int PORT = 55555;
        //IP ADDRESS to send to
        InetAddress clientIP = null;
        try {
            clientIP = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            System.out.println("Could not connect");
            e.printStackTrace();
            System.exit(0);
        }

        try {
            sending_socket = new DatagramSocket();
        } catch (SocketException e) {
            System.out.println("");
            e.printStackTrace();
            System.exit(0);
        }

        //Main loop
        boolean running = true;

        try {
            recorder = new AudioRecorder();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(AudioSender.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (running) {
            try {
                    byte[] audioBlock = recorder.getBlock();
                    ByteBuffer wrappedBlock = ByteBuffer.allocate(512);
                    wrappedBlock.put(audioBlock);
                    sending_socket.send(new DatagramPacket(wrappedBlock.array(), wrappedBlock.array().length, clientIP, PORT));
                    sending_socket.setSoTimeout(5000);
            } catch (IOException e) {
                System.out.println("ERROR: " + e);
                e.printStackTrace();
            }
        }
        sending_socket.close();
    }

    public void DatagramSocket2() {
        //Port to send to
        int PORT = 55555;
        //IP ADDRESS to send to
        InetAddress clientIP = null;
        try {
            clientIP = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            System.out.println("Could not connect");
            e.printStackTrace();
            System.exit(0);
        }

        try {
            sending_socket = new DatagramSocket();
        } catch (SocketException e) {
            System.out.println("");
            e.printStackTrace();
            System.exit(0);
        }

        //Main loop
        boolean running = true;

        try {
            recorder = new AudioRecorder();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(AudioSender.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (running) {
            try {
                //PACKET BUILDING
                packetList = new ArrayList<>();

                for (int i = 0; i < 16; i++) {
                    byte[] audioBlock = recorder.getBlock();
                    ByteBuffer wrappedBlock = ByteBuffer.allocate(514);
                    wrappedBlock.putShort(seqNum);
                    System.out.println("SENT: " + seqNum);
                    seqNum++;
                    wrappedBlock.put(audioBlock);
                    packetList.add(new DatagramPacket(wrappedBlock.array(), wrappedBlock.array().length, clientIP, PORT));
                }

                for (int i = 0; i < 4; i++) {
                    for (int j = 0; j < 4; j++) {
                        System.out.println(j * 4 + (4 - 1 - i));
                        sending_socket.send(packetList.get(j * 4 + (4 - 1 - i)));
                        sending_socket.setSoTimeout(5000);
                    }
                }

            } catch (IOException e) {
                System.out.println("ERROR: " + e);
                e.printStackTrace();
            }
        }
        sending_socket.close();
    }

    public void DatagramSocket3() {

    }

    public void DatagramSocket4() {
        //Port to send to
        int PORT = 55555;
        //IP ADDRESS to send to
        InetAddress clientIP = null;
        try {
            clientIP = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            System.out.println("Could not connect");
            e.printStackTrace();
            System.exit(0);
        }

        try {
            sending_socket = new DatagramSocket4();
        } catch (SocketException e) {
            System.out.println("");
            e.printStackTrace();
            System.exit(0);
        }

        //Main loop
        boolean running = true;

        try {
            recorder = new AudioRecorder();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(AudioSender.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (running) {
            try {

                    byte[] audioBlock = recorder.getBlock();
                    ByteBuffer wrappedBlock = ByteBuffer.allocate(514);
                    sentHashCode = (short) Arrays.hashCode(audioBlock);
                    wrappedBlock.putShort(sentHashCode);
                    wrappedBlock.put(audioBlock);
                    sending_socket.send(new DatagramPacket(wrappedBlock.array(), wrappedBlock.array().length, clientIP, PORT));
                    sending_socket.setSoTimeout(5000);

            } catch (IOException e) {
                System.out.println("ERROR: " + e);
                e.printStackTrace();
            }
        }
        sending_socket.close();
    }

//    public byte[] XOREncrypt(byte[] block){
//       ByteBuffer unwrapEncrypt = ByteBuffer.allocate(block.length);
//       ByteBuffer plainText = ByteBuffer.wrap(block);
//       int key = 2047483647;
//       
//       for(int j = 0; j < block.length / 4; j++){
//           int fourByte = plainText.getInt();
//           fourByte = fourByte ^ key;
//           unwrapEncrypt.putInt(fourByte);
//       }
//       byte[] encryptedBlock = unwrapEncrypt.array();
//         
//       return encryptedBlock;
//    }
}
