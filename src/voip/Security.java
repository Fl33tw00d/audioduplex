/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voip;

import java.io.IOException;
import static java.lang.Integer.MAX_VALUE;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 *
 * @author fleet
 */
public class Security {

    public static List<String> readFileInList(String fileName) {

        List<String> lines = Collections.emptyList();
        try {
            lines
                    = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines;
    }

    public static int generateRandomPrime() {
        List l = readFileInList("\\\\ueahome4\\stusci5\\vhe17qgu\\data\\Documents\\NetBeansProjects\\NETWORKS\\audioduplex\\primessmall.txt");
        Random rand = new Random();
        int randomPrime = Integer.parseInt((String) l.get(rand.nextInt(l.size())));
        return randomPrime;
    }

    public static void generateKeyPair() {
        int p = generateRandomPrime();
        int q = generateRandomPrime();
//        int p = 5;
//        int q = 7;
        int n = p * q;
        int z = (p - 1) * (q - 1);
        int e;
        int d;
        e = selectE(n,z);
        d = selectD(e,z);
        
        int m = 12;
        
        BigInteger message = new BigInteger(m+"");
        BigInteger cipher = message.pow(e);
        
        cipher.mod(new BigInteger(n+""));
       
        
        
    }

    public static int selectE(int n, int z) {
        ArrayList<Integer> CommonFactorsZ = new ArrayList<>();
        ArrayList<Integer> CommonFactorsE = new ArrayList<>();
        int e;
        //Find all of z common factors
        for (int i = 2; i < z; i++) {
            if (z % i == 0) {
                CommonFactorsZ.add(i);
            }
        }

        for (int i = 2; i < n; i++) {
            for (int j = 1; j < i; j++) {
                if (i % j == 0) {
                    CommonFactorsE.add(i);
                }
            }
            for (int j = 0; j < CommonFactorsZ.size() - 1; j++) {
                if (CommonFactorsZ.contains(CommonFactorsE.get(j))) {
                    CommonFactorsE.clear();
                    break;
                } else {
                    e = i;
                    return e;
                }
            }
        }
        return -1;
    }
    
    public static int selectD(int e, int z){
        int d = 0;
        for (int i = e; i < MAX_VALUE; i++) {
            if ((e * i) % z == 1 && i != e) {
                d = i;
                return d;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        generateKeyPair();
    }

}
