/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voip;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import static java.lang.Integer.MAX_VALUE;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

/**
 *
 * @author fleet
 */
public class DHSecurity {

//    public static List<String> readFileInList(String fileName) {
//
//        List<String> lines = Collections.emptyList();
//        try {
//            lines
//                    = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return lines;
//    }
//
//    public static int generateRandomPrime() {
//        List l = readFileInList("C:\\Users\\fleet\\Documents\\NetBeansProjects\\NETWORKS\\AudioDuplex\\primessmall.txt");
//        Random rand = new Random();
//        int randomPrime = Integer.parseInt((String) l.get(rand.nextInt(l.size())));
//        return randomPrime;
//    }
    public static double generateKey() {
        int p = 1024871;
        Random rand = new Random();
        int g = 145;
//        System.out.println("g: " + g);
        int x = rand.nextInt(50) + 1;
//        System.out.println("x: " + x);
        int y = rand.nextInt(50) + 1;
//        System.out.println("y: " + y);

        double R1 = (Math.pow(g, x)) % p;
        double R2 = (Math.pow(g, y)) % p;

//        System.out.println("R1: " + R1);
//        System.out.println("R2: " + R2);
        double K1 = (long)(Math.pow(R2, x)) % p;
        double K2 = (long)(Math.pow(R1, y)) % p;
//        System.out.println("K1: " + K1);
//        System.out.println("K2: " + K2);
        return K1;

    }

    public static void main(String[] args) {
        double k = generateKey();
        
//        byte[] test = generateRandomBytes(20);
//        byte[] packetBytes = generateRandomBytes(20);
//
//        byte[] cipher = XOREncrypt(packetBytes, test);
//        byte[] decipher = XORDecrypt(cipher, test);
    }

    public static byte[] generateRandomBytes(int size) {
        SecureRandom random = new SecureRandom();
        byte[] randBytes = new byte[size];
        random.nextBytes(randBytes);
        return randBytes;
    }

    public static byte[] XOREncrypt(byte[] packetBytes, byte[] keyBytes) {

        ByteBuffer unwrapEncrypt = ByteBuffer.allocate(packetBytes.length);
        ByteBuffer plainText = ByteBuffer.wrap(packetBytes);
        ByteBuffer keyBuff = ByteBuffer.wrap(keyBytes);

        for (int j = 0; j < packetBytes.length / 4; j++) {
            int fourByte = plainText.getInt();
            int key = keyBuff.getInt();
            fourByte = fourByte ^ key;
            unwrapEncrypt.putInt(fourByte);
        }
        byte[] encryptedBlock = unwrapEncrypt.array();
        return encryptedBlock;
    }

    public static byte[] XORDecrypt(byte[] packetBytes, byte[] keyBytes) {

        ByteBuffer unwrapDecrypt = ByteBuffer.allocate(packetBytes.length);
        ByteBuffer cipherText = ByteBuffer.wrap(packetBytes);
        ByteBuffer keyBuff = ByteBuffer.wrap(keyBytes);

        for (int j = 0; j < packetBytes.length / 4; j++) {
            int fourByte = cipherText.getInt();
            int key = keyBuff.getInt();
            fourByte = fourByte ^ key;
            unwrapDecrypt.putInt(fourByte);
        }
        byte[] networkedBlock = unwrapDecrypt.array();
        return networkedBlock;
    }

    public static void sendKey(byte[] keyBytes) {
        Socket socket = null;
        DataOutputStream out = null;

        try {
            socket = new Socket("localhost", 55555);
            out = new DataOutputStream(socket.getOutputStream());
        } catch (Exception e) {
            System.out.println("ERROR" + e);
        }

        try {
            out.write(keyBytes);
            out.close();
            socket.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static byte[] receiveKey() {
        try {
            ServerSocket Ssocket = new ServerSocket(55555);
            Socket socket = Ssocket.accept();
            System.out.println("Accepted");
            DataInputStream in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
            try {
                byte[] key = new byte[530];
                in.read(key);
                return key;
            } catch (IOException IO) {
                System.out.println("Error");
            }
            socket.close();
            in.close();
        } catch (Exception e) {
            System.out.println("Error");
        }
        byte[] empty = new byte[1];
        return empty;
    }
    
    
       public static byte[] XOREncryptInt(byte[] packetBytes, int key) {

        ByteBuffer unwrapEncrypt = ByteBuffer.allocate(packetBytes.length);
        ByteBuffer plainText = ByteBuffer.wrap(packetBytes);

        for (int j = 0; j < packetBytes.length / 4; j++) {
            int fourByte = plainText.getInt();
            fourByte = fourByte ^ key;
            unwrapEncrypt.putInt(fourByte);
        }
        byte[] encryptedBlock = unwrapEncrypt.array();
        reverse(encryptedBlock);
        return encryptedBlock;
    }

    public static byte[] XORDecryptInt(byte[] packetBytes, int key) {
        reverse(packetBytes);
        ByteBuffer unwrapDecrypt = ByteBuffer.allocate(packetBytes.length);
        ByteBuffer cipherText = ByteBuffer.wrap(packetBytes);

        for (int j = 0; j < packetBytes.length / 4; j++) {
            int fourByte = cipherText.getInt();
            fourByte = fourByte ^ key;
            unwrapDecrypt.putInt(fourByte);
        }
        byte[] networkedBlock = unwrapDecrypt.array();
        return networkedBlock;
    }

    
    public static void reverse(byte[] array) {
      if (array == null) {
          return;
      }
      int i = 0;
      int j = array.length - 1;
      byte tmp;
      while (j > i) {
          tmp = array[j];
          array[j] = array[i];
          array[i] = tmp;
          j--;
          i++;
      }
  }
}
