/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voip;

import CMPC3M06.AudioPlayer;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

/**
 *
 * @author vhe17qgu
 */
public class Receiver4 implements Runnable {

    static DatagramSocket receiving_socket;
    long durationSum;

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        int PORT = 55555;
        try {
            receiving_socket = new DatagramSocket4(PORT);
            receiving_socket.setSoTimeout(5000);
        } catch (SocketException e) {
            System.out.println("ERROR: Could not open UDP socket to receive from.");
            e.printStackTrace();
            System.exit(0);
        }

        boolean running = true;
        AudioPlayer player = null;

        try {
            player = new AudioPlayer();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(Receiver1.class.getName()).log(Level.SEVERE, null, ex);
        }

        CFPacket previousPacket = null;
        int count = 0;
        
        while (running && count <= 100) {
            try {
                long startTime = System.nanoTime();
                //Getting data from the packet
                byte[] buffer = new byte[528];

                DatagramPacket packet = new DatagramPacket(buffer, 0, buffer.length);

                try {
                    receiving_socket.receive(packet);
                } catch (SocketTimeoutException e) {
                    System.out.println("Socket Timed Out");
                }
                
                CFPacket inspPacket = new CFPacket(packet.getData());
                
                if (inspPacket.getHashKey() == Arrays.hashCode(inspPacket.getAudio())) {
                    System.out.println("Seq: " + inspPacket.getSequenceNo());
                    player.playBlock(inspPacket.getAudio());
                    previousPacket = inspPacket;
                }else{
                    System.out.println("Seq: " + previousPacket.getSequenceNo());
                    player.playBlock(previousPacket.getAudio());
                }
                long endTime = System.nanoTime();
                long duration = (endTime - startTime);
                durationSum += duration;
                count++;
            } catch (IOException e) {
                System.out.println("No output device connected.");
                e.printStackTrace();
            }
        }
        System.out.println("duration rec: " + durationSum / count);
        receiving_socket.close();
    }

}

//                if (inspPacket.getHashKey() == Arrays.hashCode(inspPacket.getAudio())) {
//                    System.out.println("Seq: " + inspPacket.getSequenceNo());
//                    player.playBlock(inspPacket.getAudio());
//                    previousPacket = inspPacket;
//                }else{
//                    System.out.println("Seq: " + previousPacket.getSequenceNo());
//                    player.playBlock(previousPacket.getAudio());
//                }

//                if (inspPacket.getHashKey() == Arrays.hashCode(inspPacket.getAudio())) {
//                    System.out.println("Seq: " + inspPacket.getSequenceNo());
//                    player.playBlock(inspPacket.getAudio());
//                    previousPacket = inspPacket;
//                }else{
//                    byte[] recoveredAudio = new byte[512];
//                    recoveredAudio = previousPacket.uncompressAudio();
//                    player.playBlock(recoveredAudio);
//                }