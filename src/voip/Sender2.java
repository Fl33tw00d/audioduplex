/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voip;

import CMPC3M06.AudioRecorder;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

/**
 *
 * @author vhe17qgu
 */
public class Sender2 implements Runnable {

    static DatagramSocket sending_socket;
    private int BLOCK_DIMENSION = 4;

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        //Port to send to
        int PORT = 55555;
        //IP ADDRESS to send to
        InetAddress clientIP = null;
        try {
            clientIP = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            System.out.println("Could not connect");
            e.printStackTrace();
            System.exit(0);
        }

        try {
            sending_socket = new DatagramSocket2();
            sending_socket.setSoTimeout(5000);
        } catch (SocketException e) {
            System.out.println("");
            e.printStackTrace();
            System.exit(0);
        }

        //Main loop
        boolean running = true;

        AudioRecorder recorder = null;
        try {
            recorder = new AudioRecorder();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(AudioSender.class.getName()).log(Level.SEVERE, null, ex);
        }

        int sequenceNo = 0;
        ArrayList<DatagramPacket> packetList;
        while (running && sequenceNo <= 1000) {
            try {
                packetList = new ArrayList<>();
                for (int i = 0; i < BLOCK_DIMENSION * BLOCK_DIMENSION; i++) {
                    ByteBuffer wrappedBlock = ByteBuffer.allocate(528);
                    byte[] audioBlock = recorder.getBlock();
                    int hashKey = Arrays.hashCode(audioBlock);
                    long time = System.currentTimeMillis();
                    wrappedBlock.putInt(sequenceNo);
                    wrappedBlock.putInt(hashKey);
                    wrappedBlock.putLong(time);
                    wrappedBlock.put(audioBlock);
                    DatagramPacket packet = new DatagramPacket(wrappedBlock.array(), wrappedBlock.array().length, clientIP, PORT);
                    packetList.add(packet);
                    sequenceNo++;
                }

                for (int i = 0; i < BLOCK_DIMENSION; i++) {
                    for (int j = 0; j < BLOCK_DIMENSION; j++) {
                      sending_socket.send(packetList.get(j * BLOCK_DIMENSION + (BLOCK_DIMENSION - 1 - i)));
                    }
                }

//                  for (int i = 0; i < BLOCK_DIMENSION * BLOCK_DIMENSION; i++) {
//                    sending_socket.send(packetList.get(i));
//                }

            } catch (IOException e) {
                System.out.println("ERROR: " + e);
                e.printStackTrace();
            }
        }
        sending_socket.close();
    }

}
