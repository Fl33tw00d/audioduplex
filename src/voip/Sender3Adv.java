/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package voip;

import CMPC3M06.AudioRecorder;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.LineUnavailableException;
import uk.ac.uea.cmp.voip.*;

/**
 *
 * @author vhe17qgu
 */
public class Sender3Adv implements Runnable {

    static DatagramSocket sending_socket;
    private int BLOCK_DIMENSION = 4;

    public void start() {
        Thread thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() {
        //Port to send to
        int PORT = 55555;
        //IP ADDRESS to send to
        InetAddress clientIP = null;
        try {
            clientIP = InetAddress.getByName("localhost");
        } catch (UnknownHostException e) {
            System.out.println("Could not connect");
            e.printStackTrace();
            System.exit(0);
        }

        try {
            sending_socket = new DatagramSocket3();
            sending_socket.setSoTimeout(5000);
        } catch (SocketException e) {
            System.out.println("");
            e.printStackTrace();
            System.exit(0);
        }

        //Main loop
        boolean running = true;

        AudioRecorder recorder = null;
        try {
            recorder = new AudioRecorder();
        } catch (LineUnavailableException ex) {
            Logger.getLogger(AudioSender.class.getName()).log(Level.SEVERE, null, ex);
        }

        int sequenceNo = 0;
        ArrayList<DatagramPacket> packetList;
        while (running) {
            try {
                packetList = new ArrayList<>();
                byte[] previousCompressed = new byte[128];
                byte[] compressedAudio = new byte[128];
                long startTime = System.nanoTime();
                for (int i = 0; i < BLOCK_DIMENSION * BLOCK_DIMENSION; i++) {
                    ByteBuffer wrappedBlock = ByteBuffer.allocate(656);
                    byte[] audioBlock = recorder.getBlock();
                    compressedAudio = compressAudio(audioBlock);
                    int hashKey = Arrays.hashCode(audioBlock);
                    long time = System.currentTimeMillis();
                    wrappedBlock.putInt(sequenceNo);
                    wrappedBlock.putInt(hashKey);
                    wrappedBlock.putLong(time);
                    wrappedBlock.put(previousCompressed);
                    wrappedBlock.put(audioBlock);
                    DatagramPacket packet = new DatagramPacket(wrappedBlock.array(), wrappedBlock.array().length, clientIP, PORT);
                    packetList.add(packet);
                    sequenceNo++;
                    previousCompressed = compressedAudio;
                }
                long endTime = System.nanoTime();
                long duration = (endTime - startTime);
                System.out.println("Processing Delay per packet: " + duration / 16);

                for (int i = 0; i < BLOCK_DIMENSION; i++) {
                    for (int j = 0; j < BLOCK_DIMENSION; j++) {
                        sending_socket.send(packetList.get(j * BLOCK_DIMENSION + (BLOCK_DIMENSION - 1 - i)));
                    }
                }
                
//                for (int i = 0; i < BLOCK_DIMENSION * BLOCK_DIMENSION; i++) {
//                    sending_socket.send(packetList.get(i));
//                }

            } catch (IOException e) {
                System.out.println("ERROR: " + e);
                e.printStackTrace();
            }
        }
        sending_socket.close();
    }

    public byte[] compressAudio(byte[] audio) {
        ByteBuffer compress = ByteBuffer.allocate(128);
        ByteBuffer uncompressedAudio = ByteBuffer.wrap(audio);

        for (int i = 0; i < 64; i++) {
            short soundSample;
            soundSample = uncompressedAudio.getShort(i*4);
            compress.putShort(soundSample);
        }
        return compress.array();
    }


}
